/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'primary' : '#EA40A4',
        'light' : '#EEE',
        'dark' : '#313154',
        'gray' : '#888',
        'danger' : '#ff5b57',
        'business' : '#3A82EE',
        'personal' : '#EA40A4',
        'business-glow': 'rgba(58,130,238,0.75)',
        'personal-glow': 'rgba(234,64,164,0.75)',
        'shadow': 'rgba(0,0,0,0.1)'
      }
    },
  },
  plugins: [],
}
